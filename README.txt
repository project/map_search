
Overview:
--------
Provides functions to search for routes defined in cck_maps by proximity.

Requires:
--------
 - Drupal 5.0


Credits:
-------
 - Author: Oliver Coleman, oliver@e-geek.com.au
 - http://e-geek.com.au
 - http://enviro-geek.net
